#trabalho enokida,julio,rennato
import math
from matplotlib import pyplot

def main():
    w=float(input("digite a velocidade angular em rad/s: "))
    maior=0
    menor=0
    angulo1=0
    angulo2=0
    for i in range(0,181,1):
        vaj=((w*0.125*math.sin(i*math.pi/180))+(w*0.125*(math.cos(i*math.pi/180))*(math.tan(math.asin((125/350)*(math.sin(i*math.pi/180)))))))
        if vaj>maior:
            maior=vaj
            angulo1=i
        if vaj<menor:
            menor=vaj
            angulo2=i
    if maior>=(-1*menor):
        print("a maior velocidade(em modulo) é: ",maior,"no angulo: ",angulo1)
    if maior<(-1*menor):
        print("a maior velocidade(em modulo) é: ",(-1*menor),"m/s no angulo: ",angulo1)
    teta=range(181)
    vaj=[((w*0.125*math.sin(i*math.pi/180))+(w*0.125*(math.cos(i*math.pi/180))*(math.tan(math.asin((0.125/0.350)*(math.sin(i*math.pi/180)))))))for i in teta]
    pyplot.plot(teta,vaj)
    pyplot.grid(True)
    pyplot.xlabel(r'$\theta\,(^{o})$')
    pyplot.ylabel("velocidade(m/s)")
main()